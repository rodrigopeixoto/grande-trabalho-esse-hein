
package visão;
import java.io.Serializable;


public class Pessoa implements Serializable {


    private String cpf;
    private String nome;
    private String sexo;
    private String endereco;
    private int tipoAssociado;
    Principal principal;
    //Método construtor da classe Pessoa
    
    public Pessoa(String cpf, String nome, String sexo, String endereco){ 
        super();
        this.cpf = cpf;
        this.nome = nome;
        this.sexo = sexo;
        this.endereco = endereco;
        this.principal = principal;
    }
    
    public String getCpf() {
        return this.cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSexo() {
        return this.sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEndereco() {
        return this.endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }        

    public int getTipoAssociado() {
        return tipoAssociado;
    }

    public void setTipoAssociado(int tipoAssociado) {
        this.tipoAssociado = tipoAssociado;
    }
                
    public String toString(){
        
        return "Esses são os Associados da Biblioteca: \n" + principal.vetorDeAssociados;
    
    }
}
