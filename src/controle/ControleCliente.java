/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import modelo.ModeloCliente;


public class ControleCliente {
    
    ConexaoBD conex = new ConexaoBD();
    modelo.ModeloCliente mod = new ModeloCliente();
    
    public void botaoSalvar(ModeloCliente mod){
        conex.conexao();
        try {
            PreparedStatement pst = conex.con.prepareStatement("insert into pessoa(nome, cpf, sexo, endereco) values(?,?,?,?)");
            pst.setString(1, mod.getNome());
            pst.setString(2, mod.getCpf());
            pst.setString(3, mod.getSexo());
            pst.setString(4, mod.getEndereco());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao inserir os dados! /nErro:"+ex);
        }
        
        conex.desconecta();
    
    }
    
}
